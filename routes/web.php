<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {
  Route::get('/', 'StoreController@index')->middleware('verified');
  Route::get('/home', 'StoreController@index')->middleware('verified');
  Route::get('/create', 'StoreController@create')->middleware('verified');
  Route::get('/edit/{store}', 'StoreController@edit')->middleware('verified');
  Route::post('/store', 'StoreController@store')->middleware('verified');
  Route::put('/update/{store}', 'StoreController@update')->middleware('verified');
  Route::delete('/delete/{store}', 'StoreController@delete')->middleware('verified');
});

Route::group(['prefix' => 'stations', 'middleware' => ['auth']], function () {
  Route::get('/list/{store}', 'StationController@index')->middleware('verified');
  Route::get('/create/{store}', 'StationController@create')->middleware('verified');
  Route::get('/edit/{store}/{station}', 'StationController@edit')->middleware('verified');
  Route::post('/store', 'StationController@store')->middleware('verified');
  Route::put('/update/{store}/{station}', 'StationController@update')->middleware('verified');
  Route::delete('/delete/{store}/{station}', 'StationController@delete')->middleware('verified');
});

Route::group(['prefix' => 'opening', 'middleware' => ['auth']], function () {
  Route::get('/{store}/{station?}', 'OpeningController@index')->middleware('verified');
  Route::get('/create/{day}/{store}/{station?}', 'OpeningController@create')->middleware('verified');
  Route::get('/edit/{store}/{opening}/{station?}', 'OpeningController@edit')->middleware('verified');
  Route::post('/store', 'OpeningController@store')->middleware('verified');
  Route::put('/update', 'OpeningController@update')->middleware('verified');
  Route::delete('/delete/{opening}', 'OpeningController@delete')->middleware('verified');
});

Route::group(['prefix' => 'exceptions', 'middleware' => ['auth']], function () {
  Route::get('/{store}/{station?}', 'ExceptionController@index')->middleware('verified');
  Route::get('/create/{store}/{station?}', 'ExceptionController@create')->middleware('verified');
  Route::get('/edit/{store}/{exception}/{station?}', 'ExceptionController@edit')->middleware('verified');
  Route::post('/store', 'ExceptionController@store')->middleware('verified');
  Route::put('/update', 'ExceptionController@update')->middleware('verified');
  Route::delete('/delete/{exception}', 'ExceptionController@delete')->middleware('verified');
});


Route::group(['middleware' => ['guest']], function () {
  Route::get('/', function () {
      return view('welcome');
  });
});

Auth::routes(['verify' => true]);
