<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Stores;
use App\User;
use Faker\Generator as Faker;

$factory->define(Stores::class, function (Faker $faker) {
    return [
        'tenant_id' => User::pluck('id')->random(),
        'store_name' => ucfirst($faker->words(2,true)),
        'store_address' => ucfirst($faker->words(2,true))
    ];
});
