<?php

use Illuminate\Database\Seeder;
use App\Stores;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Create 10 users
        factory(Users::class, 10)->create();

        // $this->call(UsersTableSeeder::class);
        factory(Stores::class, 100)->create();
    }
}
