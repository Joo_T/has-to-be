@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Store {{ $store->id }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($errors->any())
                      <div class="alert alert-danger">
                        <ul>
                          @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                        </ul>
                      </div>
                    @endif

                    <form method="POST" action="/update/{{ $store->id }}">

                      @method('PUT')

                      @csrf

                      <div class="form-group">
                        <label for="title">Name:</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('store_name', $store->store_name) }}" />
                      </div>

                      <div class="form-group">
                        <label for="description">Address:</label>
                        <input type="text" class="form-control" id="address" name="address" value="{{ old('store_address', $store->store_address) }}" />
                      </div>

                      <button type="submit" class="btn btn-primary">Update</button>
                      <a href="/" class="btn btn-warning float-right">Cancel</a>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
