@extends('layouts.app')

@section('scripts')
<script>
  function confirmDelete(event)
  {
    //Prevent the form from submitting automatically
    event.preventDefault();

    //Confirm the really want to delete
    if (confirm('Do you really want to delete this exception?')) {
      //submit the form
      event.target.submit();
    }
  }
</script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                  <a href="@if(!empty($station_details)) /stations/list/{{ $store->id }} @else /home @endif" class="btn btn-primary float-left">Back</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if ($errors->any())
                      <div class="alert alert-danger">
                        <ul>
                          @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                        </ul>
                      </div>
                    @endif

                    <table id="stores" class="table table-striped table-bordered">
                      <thread>
                        <tr>
                          <td><center>Exceptions for
                            @if(!empty($station_details))
                            <strong>{{ $station_details->station_name }}</strong> station
                            @else
                            <strong>{{ $store->store_name }}</strong> shop
                            @endif
                          </center></td>
                        </tr>
                      </thread>
                      <tbody>
                        @foreach($exceptions as $exception)
                          <tr>
                            <td>
                              <div class="dropdown show float-left w-100" style="margin-right:5px;">
                                <a class="btn @if($exception->status == false) btn-danger @else btn-success @endif dropdown-toggle w-100" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Stations are @if($exception->status == false) closed @else open @endif between <small>({{ date('Y-m-d H:i', strtotime($exception->start_date)) }} - {{ date('Y-m-d H:i', strtotime($exception->end_date)) }})</small><br>Explanation: {{ $exception->description }}
                                </a>

                                <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuLink">
                                  @if(!empty($station_details))
                                  <a href="/exceptions/edit/{{ $store->id }}/{{ $exception->id }}/{{ $station_details->id }}" class="btn btn-primary mb-1 w-100">Edit</a>
                                  @else
                                  <a href="/exceptions/edit/{{ $store->id }}/{{ $exception->id }}" class="btn btn-primary mb-1 w-100">Edit</a>
                                  @endif
                                  <form method="POST" action="/exceptions/delete/{{ $exception->id }}" onsubmit="confirmDelete(event)">
                                    @method('DELETE')
                                    @csrf
                                    <input type="hidden" id="store_id" name="store_id" value="{{ $store->id }}" />
                                    @if(!empty($station_details))
                                    <input type="hidden" id="station_id" name="station_id" value="{{ $station_details->id }}" />
                                    @endif
                                    <button type="submit" class="btn btn-danger bt-sm w-100">Delete</button>
                                  </form>
                                </div>
                              </div>
                            </td>
                          </tr>
                        @endforeach
                        <tr>
                          <td>
                            @if(!empty($station_details))
                            <a href="/exceptions/create/{{ $store->id }}/{{ $station_details->id }}" class="btn btn-success float-right">Add+</a>
                            @else
                            <a href="/exceptions/create/{{ $store->id }}" class="btn btn-success float-right">Add+</a>
                            @endif
                          </td>
                        </tr>
                      </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
