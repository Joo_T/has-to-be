@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add exception to
                  @if(!empty($station_details))
                  <strong>{{ $station_details->station_name }}</strong> station
                  @else
                  <strong>{{ $store->store_name }}</strong> store
                  @endif
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($errors->any())
                      <div class="alert alert-danger">
                        <ul>
                          @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                        </ul>
                      </div>
                    @endif

                    <form method="POST" action="/exceptions/store">

                      @csrf

                      <input type="hidden" class="form-control" id="store_id" name="store_id" value="{{ $store->id }}" />
                      <input type="hidden" class="form-control" id="station" name="station" value="0" />
                      @if(!empty($station_details))
                      <input type="hidden" class="form-control" id="station_id" name="station_id" value="{{ $station_details->id }}" />
                      @endif

                      <div class="form-group">
                        <label for="title" @if(!empty($errors->first('from'))) class="text-danger" @endif>From:</label>
                        <input type="date" class="form-control @if(!empty($errors->first('from'))) is-invalid @endif" id="date_from" name="from" value="{{ old('from') }}" />
                      </div>

                      <div class="form-group">
                        <label for="title" @if(!empty($errors->first('to'))) class="text-danger" @endif>To:</label>
                        <input type="date" class="form-control @if(!empty($errors->first('to'))) is-invalid @endif" id="date_to" name="to" value="{{ old('to') }}" />
                      </div>

                      <div class="form-group">
                        <label for="title" @if(!empty($errors->first('description'))) class="text-danger" @endif>Description:</label>
                        <input type="date" class="form-control @if(!empty($errors->first('description'))) is-invalid @endif" id="description" name="description" value="{{ old('description') }}" />
                      </div>

                      <div class="form-group">
                        <label for="title">Status:</label>
                        <input type="checkbox" id="status" name="status" checked data-toggle="toggle">
                      </div>

                      <button type="submit" class="btn btn-primary">Add+</button>
                      @if(!empty($station_details))
                      <a href="/exceptions/{{ $store->id }}/{{ $station_details->id }}" class="btn btn-warning float-right">Cancel</a>
                      @else
                      <a href="/exceptions/{{ $store->id }}" class="btn btn-warning float-right">Cancel</a>
                      @endif
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
