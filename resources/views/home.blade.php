@extends('layouts.app')

@section('scripts')
<script>
  function confirmDelete(event)
  {
    //Prevent the form from submitting automatically
    event.preventDefault();

    //Confirm the really want to delete
    if (confirm('Do you really want to delete this store?')) {
      //submit the form
      event.target.submit();
    }
  }
</script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                  <a href="/create" class="btn btn-primary float-right">Add Store</a>
                </div>
                <div class="card-header">
                  <center>Available sotres</center>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if ($errors->any())
                      <div class="alert alert-danger">
                        <ul>
                          @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                        </ul>
                      </div>
                    @endif

                    <div class="pager-top">{{ $stores->links() }}</div>

                    <table id="stores" class="table table-striped table-bordered">
                      <thread>
                        <tr>
                          <td>Store Name</td>
                          <td>Address</td>
                          <td>Action</td>
                        </tr>
                      </thread>
                      <tbody>
                        @foreach($stores as $store)
                          <tr>
                            <td class="align-middle">{{ $store->store_name }}</td>
                            <td class="align-middle">{{ $store->store_address }}</td>
                            <td align="middle">
                              <a href="/edit/{{ $store->id }}" class="btn btn-primary mb-1">Edit</a>
                              <br/>
                              <a href="/stations/list/{{ $store->id }}" class="btn btn-primary mb-1">Stations</a>
                              <br/>
                              <a href="/opening/{{ $store->id }}" class="btn btn-primary mb-1">General opening hours</a>
                              <br/>
                              <a href="/exceptions/{{ $store->id }}" class="btn btn-primary mb-1">Exceptoins</a>
                              <br/>
                              <form method="POST" action="/delete/{{ $store->id }}" onsubmit="confirmDelete(event)">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger bt-sm">Delete</button>
                              </form>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>

                    <div class="pager-btm">{{ $stores->links() }}</div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
