@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit <strong>{{ $opening->start_date }}-{{ $opening->end_date }}</strong> opening</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($errors->any())
                      <div class="alert alert-danger">
                        <ul>
                          @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                        </ul>
                      </div>
                    @endif

                    <form method="POST" action="/opening/update">

                      @method('PUT')

                      @csrf

                      <input type="hidden" class="form-control" id="store_id" name="store_id" value="{{ $store->id }}" />
                      <input type="hidden" class="form-control" id="opening_id" name="id" value="{{ $opening->id }}" />
                      @if(!empty($station_details))
                      <input type="hidden" class="form-control" id="station_id" name="station_id" value="{{ $station_details->id }}" />
                      @endif

                      <div class="form-group">
                        <label for="title" @if(!empty($errors->first('from'))) class="text-danger" @endif>From:</label>
                        <input type="date" class="form-control @if(!empty($errors->first('from'))) is-invalid @endif" id="time_from" name="from" value="{{ old('from', $opening->start_date) }}" />
                      </div>

                      <div class="form-group">
                        <label for="title" @if(!empty($errors->first('to'))) class="text-danger" @endif>To:</label>
                        <input type="date" class="form-control @if(!empty($errors->first('to'))) is-invalid @endif" id="time_to" name="to" value="{{ old('to', $opening->end_date) }}" />
                      </div>

                      <button type="submit" class="btn btn-primary">Update</button>
                      @if(!empty($station_details))
                      <a href="/opening/{{ $store->id }}/{{ $station_details->id }}" class="btn btn-warning float-right">Cancel</a>
                      @else
                      <a href="/opening/{{ $store->id }}" class="btn btn-warning float-right">Cancel</a>
                      @endif
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
