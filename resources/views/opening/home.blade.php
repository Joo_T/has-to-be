@extends('layouts.app')

@section('scripts')
<script>
  function confirmDelete(event)
  {
    //Prevent the form from submitting automatically
    event.preventDefault();

    //Confirm the really want to delete
    if (confirm('Do you really want to delete this opening?')) {
      //submit the form
      event.target.submit();
    }
  }
</script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                  <a href="@if(!empty($station_details)) /stations/list/{{ $store->id }} @else /home @endif" class="btn btn-primary float-left">Back</a>
                </div>
                <div class="card-header">
                  <center>General opening hours of
                    @if(!empty($station_details))
                    <strong>{{ $station_details->station_name }}</strong> station
                    @else
                    <strong>{{ $store->store_name }}</strong> shop
                    @endif
                  </center>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if ($errors->any())
                      <div class="alert alert-danger">
                        <ul>
                          @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                        </ul>
                      </div>
                    @endif

                    <table id="stores" class="table table-striped table-bordered">
                      <thread>
                        <tr>
                          <td></td>
                          <td>Opening hours</td>
                        </tr>
                      </thread>
                      <tbody>
                        @foreach($openings as $key => $times)
                          <tr>
                            <td class="align-middle">{{ $key }}</td>
                            <td align="middle">
                              @foreach($times as $time)
                              <div class="dropdown show float-left" style="margin-right:5px;">
                                <a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  {{ $time->start_date }}-{{ $time->end_date }}
                                </a>

                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                  @if(!empty($station_details))
                                  <a href="/opening/edit/{{ $store->id }}/{{ $time->id }}/{{ $station_details->id }}" class="btn btn-primary mb-1 w-100">Edit</a>
                                  @else
                                  <a href="/opening/edit/{{ $store->id }}/{{ $time->id }}" class="btn btn-primary mb-1 w-100">Edit</a>
                                  @endif
                                  <form method="POST" action="/opening/delete/{{ $time->id }}" onsubmit="confirmDelete(event)">
                                    @method('DELETE')
                                    @csrf
                                    <input type="hidden" id="store_id" name="store_id" value="{{ $store->id }}" />
                                    <input type="hidden" id="day" name="day" value="{{ $key }}" />
                                    @if(!empty($station_details))
                                    <input type="hidden" id="station_id" name="station_id" value="{{ $station_details->id }}" />
                                    @endif
                                    <button type="submit" class="btn btn-danger bt-sm w-100">Delete</button>
                                  </form>
                                </div>
                              </div>
                              @endforeach
                              @if(!empty($station_details))
                              <a href="/opening/create/{{ $key }}/{{ $store->id }}/{{ $station_details->id }}" class="btn btn-success float-right">Add+</a>
                              @else
                              <a href="/opening/create/{{ $key }}/{{ $store->id }}" class="btn btn-success float-right">Add+</a>
                              @endif
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
