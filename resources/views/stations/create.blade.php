@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Station</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($errors->any())
                      <div class="alert alert-danger">
                        <ul>
                          @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                        </ul>
                      </div>
                    @endif

                    <form method="POST" action="/stations/store">

                      @csrf

                      <input type="hidden" class="form-control" id="store_id" name="store_id" value="{{ $store->id }}" />

                      <div class="form-group">
                        <label for="title">Name:</label>
                        <input type="text" class="form-control" id="name" name="name" />
                      </div>

                      <div class="form-group">
                        <label for="title">Employee use only:</label>
                        <input type="checkbox" class="form-control" id="employee" name="employee" />
                      </div>

                      <button type="submit" class="btn btn-primary">Create</button>
                      <a href="/stations/list/{{ $store->id }}" class="btn btn-warning float-right">Cancel</a>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
