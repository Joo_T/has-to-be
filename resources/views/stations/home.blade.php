@extends('layouts.app')

@section('scripts')
<script>
  function confirmDelete(event)
  {
    //Prevent the form from submitting automatically
    event.preventDefault();

    //Confirm the really want to delete
    if (confirm('Do you really want to delete this station?')) {
      //submit the form
      event.target.submit();
    }
  }
</script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                  <a href="/" class="btn btn-primary float-left">Back</a>
                  <a href="/stations/create/{{ $store->id }}" class="btn btn-primary float-right">Add Station</a>
                </div>
                <div class="card-header">
                  <center>Available stations</center>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if ($errors->any())
                      <div class="alert alert-danger">
                        <ul>
                          @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                        </ul>
                      </div>
                    @endif

                    <div class="pager-top">{{ $stations->links() }}</div>

                    <table id="stores" class="table table-striped table-bordered">
                      <thread>
                        <tr>
                          <td>Station Name</td>
                          <td>Action</td>
                        </tr>
                      </thread>
                      <tbody>
                        @foreach($stations as $station)
                          <tr>
                            <td class="align-middle">{{ $station->station_name }} @if($station->employee) <span title="employee use only">(e)</span> @endif</td>
                            <td align="middle">
                              <a href="/stations/edit/{{ $store->id }}/{{ $station->id }}" class="btn btn-primary mb-1">Edit</a>
                              <br/>
                              <a href="/opening/{{ $store->id }}/{{ $station->id }}" class="btn btn-primary mb-1">Time table</a>
                              <br/>
                              <a href="/exceptions/{{ $store->id }}/{{ $station->id }}" class="btn btn-primary mb-1">Exceptions</a>
                              <br/>
                              <form method="POST" action="/stations/delete/{{ $store->id }}/{{ $station->id }}" onsubmit="confirmDelete(event)">
                                @method('DELETE')
                                @csrf
                                <input type="hidden" id="store_id" name="store_id" value="{{ $store->id }}" />
                                <button type="submit" class="btn btn-danger bt-sm">Delete</button>
                              </form>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>

                    <div class="pager-btm">{{ $stations->links() }}</div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
