<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stores extends Model
{
  /**
  * Get the stations for the store.
  */
  public function stations()
  {
    return $this->hasMany('App\Charging_stations', 'store_id');
  }

  /**
  * Get the general operation times for the store.
  */
  public function operationtimes()
  {
    return $this->hasMany('App\Operation_times', 'store_id');
  }

  /**
  * Get the general operation times for the store.
  */
  public function exceptions()
  {
    return $this->hasMany('App\Exceptions', 'store_id');
  }
}
