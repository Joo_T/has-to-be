<?php

/**
 * ChargingStation command
 *
 * @package   ChargingStation
 * @author    Nandor Huszar <nandor.huszar@gmail.com>
 * @copyright 2020 - Nandor Huszar
 */


/**
 * Returns with charging station status
 *
 * @package    ChargingStation
 * @subpackage Command
 * @category   Command
 * @author     Nandor Huszar <nandor.huszar@gmail.com>
 * @copyright  2020 - Nandor Huszar
 *
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Partyline;
use App\Charging_stations;
use App\Exceptions;
use App\Operation_times;
use Log;

class ChargingStation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'chargingstation
    {station_id : station ID to return a status with}
    {--timestamp= : OPTIONAL: given timestamp to check station status, default is the current time}
    {--options= : OPTIONAL: comma delimited list of options: "status", "change". The "status" and "change" is used by default }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to check charging station status';

    /**
     * The status to return
     *
     * @var boolean
     */
    public $status = null;

    /**
     * The description to return
     *
     * @var string
     */
    public $desc = '';

    /**
     * The description to return
     *
     * @var string
     */
    public $exception = false;

    /**
     * The description to return
     *
     * @var string
     */
    public $station_details = '';


    /**
     * The description to return
     *
     * @var string
     */
    public $check_date_time = "";

    /**
     * The description to return
     *
     * @var string
     */
    public $week_day = "";

    /**
     * The description to return
     *
     * @var string
     */
    public $open_end = "";

    /**
     * The description to return
     *
     * @var string
     */
    public $options = ['status', 'change'];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }//end __constructor()


    /**
     * Execute the console command.
     *
     * @param Charging_stations
     * @param Exceptions
     * @param Operation_times
     *
     * @return mixed
     */
    public function handle(Charging_stations $station, Exceptions $exception, Operation_times $operationTimes)
    {
        Partyline::bind($this);

        $station_id = $this->argument('station_id');
        $timestamp  = $this->option('timestamp');
        $options  = $this->option('options');

        //checkig parameters
        if(!$this->checkParameters($station_id, $timestamp, $options)) {
          return false;
        }

        //get details of the station
        $this->station_details = $station->find($station_id)->toArray();
        if (empty($this->station_details)) {
          Partyline::error("There is no Station found with ID: " . $station_id);
          return false;
        }

        //checkig if there is a shop or station exception given
        if (!$this->checkException($exception)) {
          //no exception, so checking particular station's opening time
          if (!$this->checkStationOpening($operationTimes)) {
            //no particular station's opening time, so checking shop opening time
            $this->checkShopOpening($operationTimes);
          }
        }

        if(in_array("status", $this->options)) {
          Partyline::info("The " . $this->station_details['station_name'] . " is " . $this->status . ", because of " . $this->desc);
        }

        if(in_array("change", $this->options)) {
          $this->checkChangeStatus($exception, $operationTimes);
        }

    }//end handle()

    /**
     * checkParameters is for checking all the given parameter and option is valid
     *
     * @param string $station_id The station ID
     * @param string $timestamp  The time we are looking for station status
     * @param array  $options    The option if we want status, change or both
     *
     * @return mixed
     */
    private function checkParameters($station_id, $timestamp, $options)
    {
      //checking station_id does exist because that is mandatory
      if (!isset($station_id) || empty($station_id)) {
        Partyline::error("Station ID is required");
        return false;
      }

      //checking if given timestamp is valid
      if (!empty($timestamp) && !$this->isValidTimeStamp($timestamp)) {
        Partyline::error("The given timestamp is not valid:" . $timestamp);
        return false;
      }

      //if there is no given timestamp, we are using the current date-time
      if (!empty($timestamp)) {
        $this->check_date_time = date('Y-m-d H:i:s', $timestamp);
        Partyline::info("Using " . $this->check_date_time . " time.");
      } else {
        Partyline::info("Using current time.");
        $this->check_date_time = date('Y-m-d H:i:s');
      }

      //checking given options are correct
      if (!empty($options)) {
        $options = explode(",", $options);
        foreach ($options as $key => $value) {
          if (!in_array($value, $this->options)) {
            unset($options[$key]);
          }
        }
      }

      //if no options are given or the given options are not correct, using default value
      if (!empty($options)) {
        $this->options = $options;
      } else {
        Partyline::info("Using default options: status-change");
      }

      return true;
    }//end checkParameters()

    /**
     * checkException is looking for if there is any exception given for the
     * station in the given date-time
     *
     * @param Exception $exception
     *
     * @return mixed
     */
    private function checkException($exception)
    {
      $all_exception = $exception->where('store_id', $this->station_details['store_id'])
        ->where('start_date', '<=' , $this->check_date_time)
        ->where('end_date', '>' , $this->check_date_time)
        ->get()
        ->toArray();

      if (!empty($all_exception) && count($all_exception) > 1) {
        //if more then 1 exception found, one is store exception and one station exception
        //we have to use the station exception, that overwrite the general shop exception
        foreach($all_exception as $exception) {
          if($exception['station'] != 0) {
            $this->status = boolval($exception['status']) ? 'open' : 'closed';
            $this->desc = $exception['description'];
            $this->exception = true;
            $this->open_ends = $exception[0]['end_date'];
          }
        }

        return true;
      } elseif (!empty($all_exception) && count($all_exception) == 1) {
        //there is only one exception found, use that
        $this->status = boolval($all_exception[0]['status']) ? 'open' : 'closed';
        $this->desc = $all_exception[0]['description'];
        $this->exception = true;
        $this->open_ends = $exception[0]['end_date'];
        return true;
      } else {
        Partyline::info("There is no exception, checking station general time");
      }

      return false;
    }//end checkException()

    /**
     * checkStationOpening is looking for general opening for the specific station
     *
     * @param Operation_times $operationTimes
     *
     * @return mixed
     */
    private function checkStationOpening($operationTimes)
    {
      $this->week_day     = date('D', strtotime($this->check_date_time));
      $this->hour_minute  = date('H:i:s', strtotime($this->check_date_time));
      $station_opening    = $operationTimes->where('station_id', $this->station_details['id'])
        ->where('day', $this->week_day)
        ->where('start_date', '<=', $this->hour_minute)
        ->where('end_date', '>', $this->hour_minute)
        ->get()
        ->toArray();

        //if no result, there is no specific opening on that station on the given day-time
        if (empty($station_opening)) {
          return false;
        }

        $this->status    = 'open';
        $this->desc      = 'General opening time: ' . $station_opening[0]['day'] . ' (' . $station_opening[0]['start_date'] . '-' . $station_opening[0]['end_date'] . ')';
        $this->open_ends = $station_opening[0]['end_date'];

        return true;
    }//end checkStationOpening()

    /**
     * checkShopOpening is looking for general opening for shop
     *
     * @param Operation_times $operationTimes
     *
     * @return mixed
     */
    private function checkShopOpening($operationTimes)
    {
      $store_opening    = $operationTimes->where('store_id', $this->station_details['store_id'])
        ->where('day', $this->week_day)
        ->where('start_date', '<=', $this->hour_minute)
        ->where('end_date', '>', $this->hour_minute)
        ->get()
        ->toArray();

        //if no result, there is no specific opening on that station on the given day-time
        if (empty($store_opening)) {
          $this->status = 'close';
          $this->desc = 'The shop is closed on the given time.';
          return false;
        }

        $this->status    = 'open';
        $this->desc      = 'General shop opening time: ' . $store_opening[0]['day'] . ' (' . $store_opening[0]['start_date'] . '-' . $store_opening[0]['end_date'] . ')';
        $this->open_ends = $store_opening[0]['end_date'];

        return true;
    }//end checkShopOpening()

    /**
     * checkChangeStatus is generating a date-time array for all exceptions and
     * openings to be able to return when the status change is going to happen
     *
     * @param Exception       $exception
     * @param Operation_times $operationTimes
     *
     * @return mixed
     */
    private function checkChangeStatus($exception, $operationTimes)
    {
        //we check max 1 week exceptions, because there is going to be an open/close meanwhile
        if ($this->exception) {
          //if we had an exception, we have to check the status change from its end
          //where this->open_end is the exception's end
          $exception_interval = date("Y-m-d H:i:s", strtotime($this->open_ends . " +1 week"));
          $date = $this->open_ends;
        } else {
          //if we had an normal opening time, we have to check the status change from the given time
          $exception_interval = date("Y-m-d H:i:s", strtotime($this->check_date_time . " +1 week"));
          $date = $this->check_date_time;
        }

        //check what exception we have to look for
        if ($this->status == "open") {
          $negate_status = 0;
        } else {
          $negate_status = 1;
        }

        //get all the exceptions in the next coming week
        $all_exception = $exception->where('store_id', $this->station_details['store_id'])
        ->where('start_date', '>' , $this->check_date_time)
        ->where('end_date', '<' , $exception_interval)
        ->where('status', $negate_status)
        ->get()
        ->toArray();

        //generate an array that will include all the dates-times for changes
        $dates_time_array = [];
        foreach ($all_exception as $exception)
        {
          $dates_time_array[$exception['start_date']] = 'exception';
        }

        //get all the openings for the shop-station
        $station_openings = $operationTimes->where('store_id', $this->station_details['store_id'])
          ->orderBy('day', 'desc')
          ->orderBy('start_date', 'desc')
          ->get()
          ->toArray();

        //amend $dates_time_array to have all the opening dates for the week as well
        for($i=0;$i<7;$i++) {
          $day = date("D", strtotime($date . " +".$i." day"));
          $day_date = date("Y-m-d", strtotime($date . " +".$i." day"));
          foreach ($station_openings as $opentime) {
            $date_key = $day_date . ' ' . $opentime['start_date'] . ':00';
            if ($opentime['day'] == $day) {
              $dates_time_array[$date_key] = $opentime['end_date'];
            }
          }
        }

        //ordering by date
        ksort($dates_time_array);
        $first_finding = array_slice($dates_time_array, 0, 1);

        $found = false;
        $next  = false;
        foreach($dates_time_array as $key => $value) {
          //if the first match is exception, the its start date is the next change date-time
          if ($value == "exception" && $found == false) {
            Partyline::info("The " . $this->station_details['station_name'] . " is going to " . (($this->status == "open") ? "close" : "open") . " at " . $key);
            $found = true;
          } elseif ($this->status == "open" && $found == false) {
            //if the first match is opening and we are looking for closing change
            Partyline::info("The " . $this->station_details['station_name'] . " is going to close at " . $value);
            $found = true;
          } elseif($this->status == "close" && $next == true) {
            //if the first match was opening and we are looking for opening so we need the next item's date
            Partyline::info("The " . $this->station_details['station_name'] . " is going to open at " . $key);
            $found = true;
            $next = false;
          } elseif ($this->status == "close" && $found == false) {
            $check = substr($key, 0, 11).$value.":00";
            //make sure we are not in the current opening time
            if ($check > $date) {
              //if the first match was opening and we are looking for opening so we need the next item's date
              Partyline::info("The " . $this->station_details['station_name'] . " is going to open at " . $key);
              $found = true;
            } else {
              $next = true;
            }
          }
        }
    }//end checkChangeStatus()

    /**
     * isValidTimeStamp is checking if the given timestamp string is valid
     *
     * @param string $timestamp
     *
     * @return mixed
     */
    private function isValidTimeStamp($timestamp)
    {
      return ((string) (int) $timestamp === $timestamp)
        && ($timestamp <= PHP_INT_MAX)
        && ($timestamp >= ~PHP_INT_MAX);
    }//end isValidTimeStamp()
}//end class
