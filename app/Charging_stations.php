<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charging_stations extends Model
{
  /**
   * Get the store that owns the station.
   */
  public function store_details()
  {
      return $this->belongsTo('App\Stores', 'store_id');
  }
}
