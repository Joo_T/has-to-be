<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exceptions;
use App\Charging_stations;
use App\Stores;
use Illuminate\Support\Facades\Auth;
use Validator;

class ExceptionController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index(Stores $store, $station = "")
    {
      //Make sure the user owns the store
      if (Auth::id() != $store->tenant_id) {
        return response('Frobidden', 403);
      }

      $query = Exceptions::query();

      $station_details = [];
      if (!empty($station)) {
        $query = $query->where('store_id', $store->id)
          ->where('end_date', '>=', date('Y-m-d'))
          ->where('station', $station)
          ->orderBy('start_date', 'asc');
        $station_details = Charging_stations::find($station);
      } else {
        $query = $query->where('store_id', $store->id)
          ->where('end_date', '>=', date('Y-m-d'))
          ->where('station', false)
          ->orderBy('start_date', 'asc');
      }


      $exceptions = $query->get();

      //Load the view
      return view('exceptions.home', compact('exceptions', 'store', 'station_details'));
    }

    public function create(Stores $store, $station = "")
    {
      $station_details = [];
      if (!empty($station)) {
        $station_details = Charging_stations::find($station);
      }

      return view('exceptions.create', compact('store', 'station_details'));
    }

    public function store(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'from'        => 'required|date_format:Y-m-d H:i',
        'to'          => 'required|date_format:Y-m-d H:i',
        'description' => 'required',
        'store_id'    => 'required',
        'station'     => 'required',
      ]);

      if ($request->from > $request->to) {
        $validator->after(function ($validator) {
          $validator->errors()->add('to', 'End time cannot be sooner than the start time');
        });
      }

      // TODO:
      // CHECKING IF THE GIVEN TIME-FRAME IS NOT OVERLAPING

      if ($validator->fails()) {
          return redirect('exceptions/create/' . $request->store_id)
                 ->withErrors($validator)
                 ->withInput();
      }

      //set up new store
      $exceptions              = new Exceptions;
      $exceptions->store_id    = $request->store_id;
      $exceptions->station     = $request->station;
      $exceptions->start_date  = $request->from;
      $exceptions->end_date    = $request->to;
      $exceptions->description = $request->description;
      $exceptions->status      = isset($request->status) ? true : false;

      $redirect_url = 'exceptions/' . $request->store_id;
      if (isset($request->station_id) && !empty(isset($request->station_id))) {
        $exceptions->station  = $request->station_id;
        $redirect_url = 'exceptions/' . $request->store_id . '/' . $request->station_id;
      }

      $exceptions->save();

      //set status message and redirect back
      $request->session()->flash('status', 'Exception is added');
      return redirect($redirect_url);
    }

    public function edit(Stores $store, Exceptions $exception, $station = "")
    {
      $station_details = [];
      if (!empty($station)) {
        $station_details = Charging_stations::find($station);
      }

      //Make sure the user owns the store
      if (Auth::id() != $store->tenant_id) {
        return response('Frobidden', 403);
      }

      return view('exceptions.edit', compact('store', 'exception','station_details'));
    }

    public function update(Request $request, Exceptions $exception)
    {
      $validator = Validator::make($request->all(), [
        'from'        => 'required|date_format:Y-m-d H:i',
        'to'          => 'required|date_format:Y-m-d H:i',
        'description' => 'required',
        'store_id'    => 'required',
      ]);

      if ($request->from > $request->to) {
        $validator->after(function ($validator) {
          $validator->errors()->add('to', 'End time cannot be sooner than the start time');
        });
      }

      // TODO:
      // CHECKING IF THE GIVEN TIME-FRAME IS NOT OVERLAPING

      if ($validator->fails()) {
          return redirect('exceptions/edit/' . $request->store_id)
                 ->withErrors($validator)
                 ->withInput();
      }

      //set up new store
      $exception              = Exceptions::find($request->id);
      $exception->start_date  = $request->from;
      $exception->end_date    = $request->to;
      $exception->description = $request->description;
      $exception->status      = isset($request->status) ? true : false;

      $exception->save();

      $redirect_url = 'exceptions/' . $request->store_id;
      if (isset($request->station_id) && !empty(isset($request->station_id))) {
        $redirect_url = 'exceptions/' . $request->store_id . '/' . $request->station_id;
      }

      //set status message and redirect back
      $request->session()->flash('status', 'Exception is updated');
      return redirect($redirect_url);
    }

    public function delete(Request $request, Exceptions $exception)
    {
      // TODO:
      //EXTRA CHECK IF WE HAVE RIGHTS TO DELETE THE OPENING TIME

      //delete store
      echo $exception->delete();

      //set status message and redirect back
      $request->session()->flash('status', 'Exception is deleted');
      return redirect('exceptions/' . $request->store_id);
    }

}
