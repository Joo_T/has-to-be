<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Charging_stations;
use App\Stores;
use Illuminate\Support\Facades\Auth;

class StationController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index(Stores $store)
    {
      //Make sure the user owns the store
      if (Auth::id() != $store->tenant_id) {
        return response('Frobidden', 403);
      }

      $stations = Stores::find($store->id)->stations()->orderBy('station_name', 'asc')->paginate(10);

      //Load the view
      return view('stations.home', compact('stations', 'store'));
    }

    public function create(Stores $store)
    {
      return view('stations.create', compact('store'));
    }

    public function store(Request $request)
    {
      //validate data
      $request->validate([
        'name'     => 'required|max:255',
        'store_id' => 'required',
      ]);

      //set up new store
      $station               = new Charging_stations;
      $station->store_id     = $request->store_id;
      $station->station_name = $request->name;
      $station->employee     = isset($request->employee) ? true : false;

      $station->save();

      //set status message and redirect back
      $request->session()->flash('status', 'Station created');
      return redirect('stations/list/' . $request->store_id);
    }

    public function edit(Stores $store, Charging_stations $station)
    {
      //Make sure the user owns the store
      if (Auth::id() != $store->tenant_id) {
        return response('Frobidden', 403);
      }

      return view('stations.edit', compact('store', 'station'));
    }

    public function update(Request $request, Stores $store, Charging_stations $station)
    {
      //Make sure the user owns the store
      if (Auth::id() != $store->tenant_id) {
        return response('Frobidden', 403);
      }

      //validate data
      $request->validate([
        'name'    => 'required|max:255',
        'store_id' => 'required',
      ]);

      //update store
      $station->station_name = $request->name;
      $station->employee     = isset($request->employee) ? true : false;

      //Assign request data to store
      $station->save();

      //set status message and redirect back
      $request->session()->flash('status', 'Station updated');
      return redirect('stations/list/' . $request->store_id);
    }

    public function delete(Request $request, Stores $store, Charging_stations $station)
    {
      //Make sure the user owns the store
      if (Auth::id() != $store->tenant_id) {
        return response('Frobidden', 403);
      }

      //delete store
      $station->delete();

      //set status message and redirect back
      $request->session()->flash('status', 'Station deleted');
      return redirect('stations/list/' . $request->store_id);
    }

}
