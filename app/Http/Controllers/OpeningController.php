<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Operation_times;
use App\Charging_stations;
use App\Stores;
use Illuminate\Support\Facades\Auth;
use Validator;

class OpeningController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index(Stores $store, $station = "")
    {
      //Make sure the user owns the store
      if (Auth::id() != $store->tenant_id) {
        return response('Frobidden', 403);
      }

      $query = Operation_times::query();

      $station_details = [];
      if (!empty($station)) {
        $query = $query->where('store_id', $store->id)->where('station_id', $station);
        $station_details = Charging_stations::find($station);
      } else {
        $query = $query->where('store_id', $store->id)->where('station_id', 0);
      }

      $operation_times = $query->get();

      $openings = config('opening');
      foreach($operation_times as $time) {
        foreach($openings as $key => $value) {
          if ($time->day == $key) {
            $openings[$key][] = $time;
          }
        }
      }

      //Load the view
      return view('opening.home', compact('openings', 'store', 'station_details'));
    }

    public function create($day, Stores $store, $station = "")
    {
      $station_details = [];
      if (!empty($station)) {
        $station_details = Charging_stations::find($station);
      }

      return view('opening.create', compact('day', 'store', 'station_details'));
    }

    public function store(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'from'     => 'required|date_format:H:i',
        'to'     => 'required|date_format:H:i',
        'store_id' => 'required',
        'day' => 'required',
      ]);

      if ($request->from > $request->to) {
        $validator->after(function ($validator) {
          $validator->errors()->add('to', 'End time cannot be sooner than the start time');
        });
      }

      // TODO:
      // CHECKING IF THE GIVEN TIME-FRAME IS NOT OVERLAPING

      if ($validator->fails()) {
          return redirect('opening/create/' . $request->day . '/' . $request->store_id)
                 ->withErrors($validator)
                 ->withInput();
      }

      //set up new store
      $opening             = new Operation_times;
      $opening->store_id   = $request->store_id;
      $opening->day        = $request->day;
      $opening->start_date = $request->from;
      $opening->end_date   = $request->to;

      $redirect_url = 'opening/' . $request->store_id;
      if (isset($request->station_id) && !empty(isset($request->station_id))) {
        $opening->station_id   = $request->station_id;
        $redirect_url = 'opening/' . $request->store_id . '/' . $request->station_id;
      }

      $opening->save();

      $request->session()->flash('status', 'Opening time added to ' . $request->day);
      return redirect($redirect_url);
    }

    public function edit(Stores $store, Operation_times $opening, $station = "")
    {
      $station_details = [];
      if (!empty($station)) {
        $station_details = Charging_stations::find($station);
      }

      //Make sure the user owns the store
      if (Auth::id() != $store->tenant_id) {
        return response('Frobidden', 403);
      }

      return view('opening.edit', compact('store', 'opening', 'station_details'));
    }

    public function update(Request $request, Operation_times $opening)
    {
      $validator = Validator::make($request->all(), [
        'from'     => 'required|date_format:H:i',
        'to'     => 'required|date_format:H:i',
      ]);

      if ($request->from > $request->to) {
        $validator->after(function ($validator) {
          $validator->errors()->add('to', 'End time cannot be sooner than the start time');
        });
      }

      // TODO:
      // CHECKING IF THE GIVEN TIME-FRAME IS NOT OVERLAPING

      if ($validator->fails()) {
          return redirect('opening/edit/' . $request->store_id . '/' . $request->id)
                 ->withErrors($validator)
                 ->withInput();
      }

      //set up new store
      $opening = Operation_times::find($request->id);
      $opening->start_date = $request->from;
      $opening->end_date   = $request->to;

      $opening->save();

      $redirect_url = 'opening/' . $request->store_id;
      if (isset($request->station_id) && !empty(isset($request->station_id))) {
        $redirect_url = 'opening/' . $request->store_id . '/' . $request->station_id;
      }

      //set status message and redirect back
      $request->session()->flash('status', 'Opening time updated to ' . $request->day);
      return redirect($redirect_url);
    }

    public function delete(Request $request, Operation_times $opening)
    {
      // TODO:
      //EXTRA CHECK IF WE HAVE RIGHTS TO DELETE THE OPENING TIME

      //delete store
      $opening->delete();

      $redirect_url = 'opening/' . $request->store_id;
      if (isset($request->station_id) && !empty(isset($request->station_id))) {
        $redirect_url = 'opening/' . $request->store_id . '/' . $request->station_id;
      }

      //set status message and redirect back
      $request->session()->flash('status', 'Opening deleted');
      return redirect($redirect_url);
    }

}
