<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stores;
use Illuminate\Support\Facades\Auth;

class StoreController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index()
    {
      //Load stores
      $stores = Stores::where(['tenant_id' => Auth::user()->id])->orderBy('store_name', 'asc')->paginate(10);

      //Load the view
      return view('home', compact('stores'));
    }

    public function create()
    {
      return view('create');
    }

    public function store(Request $request)
    {
      //validate data
      $request->validate([
        'name'       => 'required|max:255',
        'address' => 'required|max:255',
      ]);

      //set up new store
      $store = new Stores;
      $store->tenant_id     = Auth::id();
      $store->store_name    = $request->name;
      $store->store_address = $request->address;

      $store->save();

      //set status message and redirect back
      $request->session()->flash('status', 'Store created');
      return redirect('home');
    }

    public function edit(Stores $store)
    {
      //Make sure the user owns the store
      if (Auth::id() != $store->tenant_id) {
        return response('Frobidden', 403);
      }

      return view('edit', compact('store'));
    }

    public function update(Request $request, Stores $store)
    {
      //Make sure the user owns the store
      if (Auth::id() != $store->tenant_id) {
        return response('Frobidden', 403);
      }

      //validate data
      $request->validate([
        'name'    => 'required|max:255',
        'address' => 'required|max:255',
      ]);

      //update store
      $store->store_name    = $request->name;
      $store->store_address = $request->address;
      //Assign request data to store
      $store->save();

      //set status message and redirect back
      $request->session()->flash('status', 'Store updated');
      return redirect('home');
    }

    public function delete(Request $request, Stores $store)
    {
      //Make sure the user owns the store
      if (Auth::id() != $store->tenant_id) {
        return response('Frobidden', 403);
      }

      //delete store
      $store->delete();

      //set status message and redirect back
      $request->session()->flash('status', 'Store deleted');
      return redirect('home');
    }

}
