<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exceptions extends Model
{
  /**
   * Get the store that owns the operation time.
   */
  public function store()
  {
      return $this->belongsTo('App\Stores');
  }
}
